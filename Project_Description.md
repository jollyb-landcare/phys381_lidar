---
title:  PHYS381 project proposal
author: Ben Jolly
date:   2017-07-25
abstract:
---

# Introduction

Landcare Research has acquired all the base components needed for a mobile lidar
(laser scanner) that can be carried by a person or mounted to a stationary
tripod, a vehicle, or a Remotely Piloted Aircraft System (RPAS). We have
previously built systems for combining data from lidars and rudimentary sensors
into a point cloud, but now need some help to do this on a larger scale with
better equipment.

![](images/vlp16.png){width=50%}
![](images/xc_rpas_vlp.jpg){width=50%}


The basic concept of a laser scanner is a device that fires a pulse of laser
light at an object and measures the time of flight between the scanner and the
object. In order to get a detailed scan of an object or scene, thousands or
millions of pulses are used while the scanner moves around/over the object. For
this to work, it is neccesary to know **exactly** where the scanner is and where
it is pointing in 3-D space for every single pulse. Given this information,
constructing a point cloud (a cloud of points in 3-D space representing an
object or scene) is conceptually straightforward but can prove surprisingly
complicated when presented with real-world challenges and sensor limitations.
Positional information is typically provided through some combination of GNSS
(Global Navigation Satellite System a.k.a. 'GPS') and IMU (Inertial Measurement
Unit) data. Participants will need to interact with both raw and post-processed
GNSS/IMU data, however will not be expected to post-process it themselves.

This would primarily be a 'soft' project, where the hardware is already set up
and assembled. The missing pieces are the logging software to actually record
incoming data, and the post-processing software to take the raw data streams and
turn them into coherent point clouds (3D models). It would be helpful for
participants to have a good working knowledge of Linux systems and be able to
program in Python and/or C, however this previous knowledge isn't a requirement
if the participants are willing and able to learn as they go. We can provide a
crash-course in Linux and Python as well as (some limited) ongoing support if
needed. Some (relatively) basic 3D trigonometry will be required to construct a
coherent point cloud from the raw laser pulses and positional information. We
would **prefer** that MATLAB is **not** used for this (Python would be the
language of choice for the post-processing component) but this is negotiable.

# Components/system description

The system consists of:

 * 1x Velodyne VLP-16 lidar 'puck'
 * 1x Advanced Navigation 'Spatial Dual' GNSS/IMU
 * 1x Raspberry Pi 2 (B+) OR 1x BeagleBone Green

Unfortunately this kit is both rather expensive and located in Palmerston North.
We are reluctant to leave it either at Lincoln (Landcare headquarters) or at UC,
so most of the development will need to happen remotely via an SSH connection.
Someone will usually be sitting next to it during normal business hours
to do any physical actions (i.e. hard reset) and help troubleshoot if required.
The kit will be bolted together and photographs/videos and dimensional drawings
will be supplied as the exact physical setup must be known in order to use the
IMU data to construct the point cloud.

![](images/spatial_dual_1.png){width=50%}
![](images/lr_rpas_gnss.jpg){width=50%}

The Spatial Dual GNSS/IMU unit has a high data rate RS232 connection (1M baud)
and will connect via USB converter. Code for fetching and logging this raw data
stream must be written. These log files will eventually be post-processed by a
third-party website so should be in raw binary format without any extra
information. The Velodyne lidar puck connects via ethernet and spams UDP
broadcast packets that can be logged using a tool like WireShark. Log files need
to go onto a USB stick, ideally logging would start when the USB stick was
connected (we can help with this as required). No further processing is expected
to happen on the Pi.

The log files will be downloaded to the participants' own computers for further
processing (an API is available for interpreting the raw binary GNSS/IMU data).
Depending on progress, participants may need to ingest a third set of
(post-processed) log files. It would be useful if participants had access to
reasonably high-powered machines (8 - 16 GB RAM with i7 processor and an SSD)
for the post-processing steps, however slower machines will still work OK. A
suggested software setup on the participants computers (lab or personal) would
be MobaXterm for the SSH connection and to copy files (if using Windows), and
Anaconda (or Miniconda) as a Python environment/package manager. It should be
possible to install \*conda without administrator privileges on any system,
however we do not recommend putting it onto a network drive (local or USB only).
A GUI text editor with syntax highlighting (such as Atom/Notepad++/Sublime Text
3) would also be a good idea. Participants will have full admin control over the
Pi and can install whatever tools they want (it will have internet access). Any
interaction they have with the networks the Pi is connected to (though they are
secure) will be monitored by our IT department so please keep that in mind.

User manuals for the VLP-16 lidar puck and the Spatial Dual will be added to the
BitBucket repository, start by reading the VLP-16 manual which will give an idea
of how much work may be required to build the point cloud from the raw
scanner data. The Spatial Dual manual has a good overview of GNSS concepts.


# Objectives

We have split this project up into several objectives (in order of importance):

 1. Log incoming data from VLP-16 lidar unit
 2. Produce simple point cloud from (static) lidar unit
 3. Log incoming data from Spatial Dual GNSS/IMU
 4. Produce point cloud from VLP-16 informed by raw IMU data from Spatial Dual (lidar will slowly be tilted as it is running)
 5. Automate steps 1 and 3 (i.e. run on boot if USB stick plugged in)
 6. Produce point cloud from VLP-16 informed by post-processed GNSS and IMU data from Spatial Dual (lidar will be mounted on a mobile platform and moved around)

Steps 1 and 3 will require the use of bash scripts and maybe some C/Python
coding on the Pi itself. Step 2 will require some Python (or similar) coding on
participants' own computers with some very simple trigonometry. Step 4 is like
step 2 but needs a bit more trig to the transforms correct. Step 5 is purely
Linux system stuff with maybe another bash script, we can help if required. Step
6 is a beefed up version of step 4 ingesting a different set of log files for
the GNSS/IMU. None of the math should be particularly difficult by itself, this
is more of a systems and data processing/analysis challenge.

If Objective 6 is acheived with time to spare there will be room to grow the
project into the Simultaneous Location and Mapping (SLAM) space. This is where
raw information from the lidar can be used to try and further increase the
positional accuracy of the positional information - particularly critical if
GNSS coverage is poor is some areas that the lidar passes through.
