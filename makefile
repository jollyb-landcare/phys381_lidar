name = $(shell basename $(CURDIR))
name = phys381
#acc = $(HOME)/writing/acc
#styles = $(acc)/styles

.clean:
	latexmk -C

.all:
	proposal

proposal:
	pandoc \
		--from markdown+yaml_metadata_block+table_captions \
		--to latex \
		--variable documentclass=article \
    	--variable classoption=11pt \
		--variable geometry="left=2cm,right=2cm,top=2cm,bottom=2cm" \
		--number-sections \
		--listings \
		--bibliography library.bib \
		--csl american-geophysical-union.csl \
		--output Project_Description.tex \
		--standalone \
		Project_Description*.md
	latexmk -pdflatex="pdflatex --synctex=l --shell-escape %O %S" -pdf Project_Description.tex
